from pomocnicze import losuj
from nazwy import NAZWY_SWIATOW

class Swiat(object):
    def __init__(self, nazwa=None):
        self.nazwa = nazwa or losuj(NAZWY_SWIATOW)
        self.postacie = []
            
