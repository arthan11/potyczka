from przedmioty import *

# typy broni
BRON_RECZNA = 1
BRON_DRZEWCOWA = 2
BRON_MIOTAJACA = 3
TARCZA = 4

BRONIE = [
    # typ, nazwa, kostki, modyfikator, wymagana sila, czy rzucana, czy dwureczna
    [BRON_RECZNA, 'Sztylet', 1, -1, 0, True, False],

    [BRON_RECZNA, 'Rapier', 1, 0, 9, False, False],
    [BRON_RECZNA, 'Palka', 1, 0, 9, True, False],
    [BRON_RECZNA, 'Mlot bojowy', 1, 1, 10, False, False],
    [BRON_RECZNA, 'Kordelas', 2, -2, 10, False, False],
    [BRON_RECZNA, 'Miecz krotki', 2, -1, 11, False, False],
    [BRON_RECZNA, 'Bulawa', 2, -1, 11, True, False],
    [BRON_RECZNA, 'Topor maly', 1, +2, 11, True, False],
    [BRON_RECZNA, 'Miecz ciezki', 2, 0, 12, False, False],
    [BRON_RECZNA, 'Korbacz', 2, 1, 13, False, False],
    [BRON_RECZNA, 'Miecz dwureczny', 3, -1, 14, False, True],
    [BRON_RECZNA, 'Topor bojowy', 3, 0, 15, False, True],
]

class Bron(Przedmiot):
    def __init__(self, stale):
        super(Bron, self).__init__(stale[1])
        self.typ = stale[0]
        self.kostki = stale[2]
        self.modyfikator = stale[3]
        self.wymagana_sila = stale[4]
        self.rzucana = stale[5]
        self.dwureczna = stale[6]
        
    def sila_txt(self):
        ret = 'K6'
        if self.kostki > 1:
            ret = '{}{}'.format(self.kostki, ret)
        if self.modyfikator > 0:
            ret = '{}+{}'.format(ret, self.modyfikator)
        elif self.modyfikator < 0:
            ret = '{}{}'.format(ret, self.modyfikator)
        return ret

    def __str__(self):
        return '<{} {}, S:{}>'.format(self.nazwa, self.sila_txt(), self.wymagana_sila)

