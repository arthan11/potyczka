
NAZWY_SWIATOW = [
    'Anadia',
    'Coliar',
    'Toril',
    'Karpri',
    'Chandos',
    'Glyth',
    'Garden',
    'H''Catha'
]

NAZWY_POSTACI_IMIONA = [
    'hudur', 
    'zuser', 
    'nurgen', 
    'beldig', 
    'groror', 
    'vol',
    'rodrerth',
    'tum',
    'dejod-kaor',
    'dam-vuh',
    'rolvodjold',
    'jotvac',
    'fui',
    'tih',
    'hitemi',
    'reron',
]

NAZWY_POSTACI_NAZWISKA = [
    'cihlil',
    'julod',
    'Dragonarm',
    'Spiritglow',
    'ditsk',
    'kulusk',
    'Mistkiller',
    'Heavyvale',
    'fulrinzab',
    'menkheb',
    'ikoguni',
    'tilzanga',
    'nei',
    'wu',
    'fenzornu',
    'mogosqor',
]
