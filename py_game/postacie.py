import random
from pomocnicze import losuj, rzut
from nazwy import NAZWY_POSTACI_IMIONA, NAZWY_POSTACI_NAZWISKA
from bronie import *

class Postac(object):
    def __init__(self, swiat, nazwa=None):
        self.swiat = swiat
        self.nazwa = nazwa or (losuj(NAZWY_POSTACI_IMIONA).title() + ' ' + losuj(NAZWY_POSTACI_NAZWISKA).title())  
        self.swiat.postacie.append(self)
        self.sila_bazowa = 8
        self.sila = self.sila_bazowa
        self.zrecznosc = 8
        self.karma = 8
        self.ruch = 10
        self.przedmioty = []
        
        self.losowo()
        
    def __str__(self):
        return '<{} S:{}({}), Z:{}(ZZ:{}), PR:{}>'.format(self.nazwa, self.sila, self.sila_bazowa, self.zrecznosc, self.zz(), self.ruch)
     
    def zz(self):
        ret = self.zrecznosc
        if self.sila <= 3:
            ret -= 3
        return ret
    
    def losowo(self):
        # rozdzielenie punktow karmy
        ile_sily = random.randint(0, self.karma)
        self.sila_bazowa += ile_sily
        self.sila = self.sila_bazowa
        self.zrecznosc += self.karma - ile_sily
        self.karma = 0
        
        # najlepszy ekwipunek wg statystyk
        bronie = []
        sila = self.sila_bazowa
        while (not bronie) and (sila >= 0):
            for bron in BRONIE:
                if bron[4] == sila:
                    bronie.append(bron)
            sila -= 1
        self.przedmioty.append(Bron(losuj(bronie)))

    def czy_trafiono(self):
        wynik = rzut(3)
        ret = wynik <= self.zz()
        print 'zz:', self.zz()
        print 'rzut:', wynik
        #print ret
        return ret
        
    def ile_trafiono(self, bron):
        wynik = rzut(bron.kostki, bron.modyfikator)
        return wynik
    
    def zyje(self):
        return self.sila > 0
