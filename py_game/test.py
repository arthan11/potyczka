from postacie import Postac
from swiaty import Swiat

if __name__ == '__main__':
    swiat = Swiat()
    postac1 = Postac(swiat)
    postac2 = Postac(swiat)
    #przedmiot = Bron(losuj(BRONIE))
    #print przedmiot
    
    print 'swiat: ' + swiat.nazwa
    for postac in swiat.postacie:
        print '  postac:', postac
        if postac.przedmioty:
            for przedmiot in postac.przedmioty:
                print '   -', przedmiot
    
    print '{} VS {}'.format(postac1.nazwa, postac2.nazwa)
    while postac1.zyje() and postac2.zyje():
        #if True:
        czy_trafiono = postac1.czy_trafiono()
        if czy_trafiono:
            ile_trafiono = postac1.ile_trafiono(postac1.przedmioty[0])
            print postac1.przedmioty[0], ile_trafiono
            postac2.sila -= ile_trafiono
        
        czy_trafiono = postac2.czy_trafiono()
        if czy_trafiono:
            ile_trafiono = postac2.ile_trafiono(postac2.przedmioty[0])
            print postac2.przedmioty[0], ile_trafiono
            postac1.sila -= ile_trafiono
        print '[{}] {}'.format(postac1.sila, postac1.nazwa)
        print '[{}] {}'.format(postac2.sila, postac2.nazwa)
        