Board = function(parent) {
  this.parent = parent
  this.object = null;
  this.record_sheet_object = null;
  this.megahexes = [];
  this.hex_width = 100;
  this.hex_height = 87;
  this.hex_top = -24;
  this.hex_left = -70;
  this.width = 0;
  this.height = 0;
  this.characters = [];

  this.clear = function() {
    for (var i = 0; i < this.megahexes.length; i++) {
      for (var j = 0; j < this.megahexes[i].hexes.length; j++) {
        this.megahexes[i].hexes[j].object.remove();
      };
    };
    this.megahexes = [];
    this.remove_characters();
  };

  this.create_board = function() {
    this.object = $( '<div class="board"></div>' ).appendTo(parent);

    this.title_object = $('<div class="title"></div>');
    this.title_object.appendTo(this.object);
  };

  this.create_megahexes = function(width, height) {
    this.width = width;
    this.height = height;
    for (var j = 0; j < width; j++) {
      for (var i = 0; i < height; i++) {
        var mh_id = this.megahexes.length;
        var mh = new MegaHex(this, mh_id, j, i);
        this.megahexes.push(mh);
      };
    };
    for (var j = 0; j < this.megahexes.length; j++) {
      for (var i = 0; i < this.megahexes[j].hexes.length; i++) {
        this.megahexes[j].hexes[i].set_position();
      };
    };
  };

  this.hide_hex = function(mh_id, id) {
    this.megahexes[mh_id].hexes[id].hide();
  };

  this.create_character = function(mh_id, hex_id, img) {
    var id = 0;
    if (this.characters.length > 0) {
      id = this.characters[this.characters.length-1].id + 1;
    };
    var char = new Char(this.megahexes[mh_id].hexes[hex_id], id, img);
    this.characters.push(char);
    return char;
  };

  this.remove_characters = function() {
    while (this.characters.length > 0) {
      this.characters[0].destroy();
    };
    this.characters = [];
  };

  this.ask_character = function() {
    this.dialog = $(
    `
    <div id="dialog" title="Tworzenie postaci">
      <form>
        <fieldset>
          <label for="name">Imię</label>
          <input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all">
          <label for="race">Rasa</label>
          <input type="text" name="race" id="race" value="człowiek" class="text ui-widget-content ui-corner-all">
          <label for="str">Siła</label>
          <input id="str" name="str" value="8" min=8 max=16>
          <label for="dex">Zręczność</label>
          <input id="dex" name="dex" value="8" min=8 max=16>

          <!-- Allow form submission with keyboard without duplicating the dialog button -->
          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
      </form>
    </div>
    `);
    this.object.appendTo(this.object);

    this.dialog.find('#str').spinner();
    this.dialog.find('#dex').spinner();

    this.dialog.dialog({
      width: 500,
      height: 500,
      autoOpen: true,
      resizable: false,
      modal: true,
      show: {
        effect: "blind",
        duration: 500
      },
      hide: {
        effect: "blind",
        duration: 500
      },
      buttons: {
        Anuluj: function() {
          $( this ).dialog( "close" );
        },
        Zatwierdź: function() {
          $( this ).dialog( "close" );
        }
      }
    });

  };

  this.set_color = function(color) {
    this.object.css('background-color', color);
  };

  this.set_title = function(title, color='black') {
    this.title_object.text(title);
    this.title_object.css('color', color);
  };

  this.x_y_2_id = function(x, y) {
    return y * this.height + x;
  };

  this.create_board();
};