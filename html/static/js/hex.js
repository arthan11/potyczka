Hex = function(parent, id) {
  this.parent = parent;
  this.id = id;
  this.object = null;
  this.visible = true;
  this.type = null;

  this.set_type = function(type) {
    this.type = type;
    if (type !== null) {
      this.object.html('<div class="hex hex_' + type + '"></div>');
    } else {
      this.object.html('');
    };
  };

  this.set_position = function() {
    var w = this.parent.parent.hex_width;
    var h = this.parent.parent.hex_height;
    var t = this.parent.parent.hex_top;
    var l = this.parent.parent.hex_left;
    var w_max = this.parent.parent.width;
    var h_max = this.parent.parent.height;

    var mh_id = this.parent.id;
    var mh_x = this.parent.x;
    var mh_y = this.parent.y;

    //var x = l + mh_x*w*2.25 + w + mh_y*w*0.75;
    //var y = t + mh_y*h*2.5 + h + mh_x*h*0.5;
    var x = (w_max*w*0.75) + l + (mh_x*w*2.25) - (mh_y*h*0.85)// + mh_x*w*2.25 + w + mh_y*w*0.75;
    var y = (h_max*w*0.5) + t + (mh_y*h*2.5) - (mh_x*h*0.5)// + 786 - (mh_y*h*2.5 + h + mh_x*h*0.5);

    if (this.id == 1) {
      x = x - (w*0.75);
      y = y - (h*0.5);
    } else if (this.id == 2) {
      y = y - h;
    } else if (this.id == 3) {
      x = x + (w*0.75);
      y = y - (h*0.5);
    } else if (this.id == 4) {
      x = x + (w*0.75);
      y = y + (h*0.5);
    } else if (this.id == 5) {
      y = y + h;
    } else if (this.id == 6) {
      x = x - (w*0.75);
      y = y + (h*0.5);
    };

    this.x = x;
    this.y = y;
    $( "#hex_" + mh_id + "_" + this.id ).css({'left': this.x, 'top': this.y});
  };

  this.create_hex = function() {
    this.object = $( '<div class="hex" id="hex_' + this.parent.id + '_'+ this.id +'"></div>' ).appendTo(parent.parent.object);
    if (this.id > 0) {
      this.object.addClass('hex' + this.id);
    }
    //this.set_position();
  };

  this.hide = function() {
    this.object.hide();
    this.visible = false;
  };

  this.show = function() {
    this.object.show();
    this.visible = true;
  };

  this.create_hex();
};
