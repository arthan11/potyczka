MegaHex = function(parent, id, y, x) {
  this.parent = parent;
  this.id = id;
  this.x = x;
  this.y = y;
  this.hexes = [];

  this.create_hexes = function() {
    for (var i = 0; i < 7; i++) {
      var hex = new Hex(this, i);
      this.hexes.push(hex);
    };
  };
  
  this.create_hexes();
};
