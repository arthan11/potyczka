
Dice = function(parent, x=0, y=0, color='white') {
  this.parent = parent;
  this.owner = parent.object;
  this.container = null;
  this.object = null;
  this.x = x;
  this.y = y;
  this.color = color;

  this.create_object = function() {

  this.container = $(`
    <div class="container">
      <div class="cube">
        <figure class="side1">
          <div class="circle center"></div>
        </figure>
        <figure class="side2">
          <div class="circle circle_2_1"></div>
          <div class="circle circle_2_2"></div>
        </figure>
        <figure class="side3">
          <div class="circle circle_2_1"></div>
          <div class="circle center"></div>
          <div class="circle circle_2_2"></div>
        </figure>
        <figure class="side4">
          <div class="circle circle_2_1"></div>
          <div class="circle circle_2_2"></div>
          <div class="circle circle_4_2"></div>
          <div class="circle circle_4_3"></div>
        </figure>
        <figure class="side5">
          <div class="circle circle_2_1"></div>
          <div class="circle circle_2_2"></div>
          <div class="circle circle_4_2"></div>
          <div class="circle circle_4_3"></div>
          <div class="circle center"></div>
        </figure>
        <figure class="side6">
          <div class="circle circle_2_1"></div>
          <div class="circle circle_2_2"></div>
          <div class="circle circle_4_2"></div>
          <div class="circle circle_4_3"></div>
          <div class="circle circle_6_3"></div>
          <div class="circle circle_6_4"></div>
        </figure>
      </div>
    </div>
    `);
    this.container.appendTo(this.owner);
    this.object = this.container.find(".cube")

    if (this.x != 0) {
      this.container.css({'left': this.x});
    };
    if (this.y != 0) {
      this.container.css({'top': this.y});
    };
    if (this.color == 'black') {
      this.object.children().css({'background-color': this.color, 'border': '2px solid white'});
      this.object.find(".circle").css({'background-color': 'white'});
    };
    this.object.children().css({'background-color': this.color});
  };

  this.show = function(side_nr, duration=200) {
    rotate_x = 0;
    rotate_y = 0;
    switch(side_nr) {
      case 2:
        rotate_x = -180;
        break;
      case 3:
        rotate_y = -90;
        break;
      case 4:
        rotate_y = 90;
        break;
      case 5:
        rotate_x = -90;
        break;
      case 6:
        rotate_x = 90;
        break;
    };

    if (this === this.parent.dices[0]) {
      this.parent.sequence.push({e: this.object, p: {'rotateX': rotate_x + "deg", 'rotateY': rotate_y + "deg"}, o:{ duration: duration } });
    } else {
      this.object.velocity({
        'rotateX': rotate_x + "deg",
        'rotateY': rotate_y + "deg"
      }, { duration: duration });
    };
  };

  this.roll = function(side_nr, duration=600) {
    var times = duration / 180;
    for (var i = 0; i < times-1; i++) {
      var side = Math.floor((Math.random() * 6) + 1);
      var new_side = Math.floor((Math.random() * 5) + 1);
      if (new_side >= side) {
        side = new_side + 1;
      } else {
        side = new_side;
      };
      this.show(side, duration=180);
    };
    this.show(side_nr, duration=180);
  };

  this.create_object();

};


DiceRoller = function(owner, x=0, y=0, max_dices=3) {
  this.owner = owner;
  this.object = null;
  this.x = x;
  this.y = y;
  this.max_dices = max_dices;
  this.dices = []
  this.sequence = []

  this.create_dice_roller = function() {
    this.object = $( '<div class="hidden dice_roller"><div class="mod"></div><div class="text hidden"></div></div>' );
    this.object.appendTo(this.owner);
    for (var i=0;i < max_dices; i++) {
      this.dices.push(new Dice(this, x=60+70*(i), y=20, color='LightYellow'));
    };
  };

  this.show_dices = function(dices, mod) {
    var x = (310 - (50 * dices + 20 * (dices - 1))) / 2;
    if (mod !== 0) {
      x -= 35;
    }
    for (var i = 0; i < this.dices.length; i++) {
      if (i < dices) {
        var container = this.dices[i].container;
        this.dices[i].object.removeClass('hidden');
        container.css({'left':x+70*i});
      } else {
        this.dices[i].object.addClass('hidden');
      };
    }
    this.object.children('.mod').css({'left': x+70*dices});

  };

  this.roll = function(duration=600, dices=0, mod=0, scores=[]) {
    this.sequence = []
    this.sequence.push({e: this.object, p: "transition.expandIn"})
    this.object.children('.text').text('');
    if (mod == 0) {
      this.object.children('.mod').text('');
    } else if (mod > 0) {
      this.object.children('.mod').text('+' + mod);
    } else {
      this.object.children('.mod').text(mod);
    };
    this.show_dices(dices, mod);

    var max_dices = dices;
    if ((max_dices == 0) || (max_dices > this.dices.length)) {
      max_dices = this.dices.length;
    };
    var sum = mod;
    var ret = 0;
    for (var i = 0; i < max_dices; i++) {
      if (scores.length > i) {
        ret = scores[i];
      } else {
        ret = Math.floor((Math.random() * 6) + 1);
      }
      sum += ret;
      this.dices[i].roll(ret, duration);
    };
    if (sum < 0) {
      sum = 0;
    };

    dice_roller.object.children('.text').text(sum);
    this.sequence.push({e: dice_roller.object.children('.text'), p: "transition.bounceIn"})
    this.sequence.push({e: dice_roller.object.children('.text'), p: "transition.bounceOut"})
    this.sequence.push({e: this.object, p: "transition.expandOut"})
    $.Velocity.RunSequence(this.sequence);
  };

  this.create_dice_roller();
};
