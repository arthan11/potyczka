Game = function() {
  this.create_game = function() {
    this.board = new Board("body");
    this.init_board();
  };

  this.init_board = function() {
    this.board.clear();
    this.board.create_megahexes(3, 5);
    this.board.hide_hex(4, 2);
    this.board.hide_hex(4, 3);
    this.board.hide_hex(4, 4);
    this.board.hide_hex(10, 1);
    this.board.hide_hex(10, 5);
    this.board.hide_hex(10, 6);
  };

  this.set_types = function(list, type) {
    for (var i=0; i < list.length; i++) {
      this.board.megahexes[list[i][0]].hexes[list[i][1]].set_type(type);
    };
  };

  this.set_types_center = function(list, type) {
    for (var i=0; i < list.length; i++) {
      this.board.megahexes[list[i]].hexes[0].set_type(type);
    };
  };


  this.set_types_megahex = function(list, type) {
    for (var i=0; i < list.length; i++) {
      for (var j=0; j < 7; j++) {
        this.board.megahexes[list[i]].hexes[j].set_type(type);
      };
    };
  };

  // MQ01-3
  this.IndigoRoom = function() {
    this.init_board();
    this.board.set_title('Indigo Room');
    this.board.set_color('indigo');
    var blacks = [
      [0, 5],
      [1, 4],
      [1, 5],
      [2, 4],
      [3, 2],
      [6, 0],
      [7, 3],
      [8, 4],
      [9, 4],
      [11, 0],
      [12, 6],
      [13, 1],
      [13, 6],
      [14, 0]
    ];
    this.set_types(blacks, 'black');
    this.board.create_character(7, 2, 25);
    this.board.create_character(7, 5, 25);
  };

  // MQ01-11
  this.GoldRoom = function() {
    this.init_board();
    this.board.set_color('gold');
    this.board.set_title('Gold Room');
    this.set_types_megahex([6, 7, 8], 'black');
    this.board.create_character(1, 5, 22);
    this.board.create_character(12, 1, 22);
  };

  // MQ01-12
  this.SilverRoom = function() {
    this.init_board();
    this.board.set_color('silver');
    this.board.set_title('Silver Room');
    this.board.create_character(14, 0, 1);
    this.board.create_character(14, 4, 2);
    this.board.create_character(14, 5, 3);
  };

  // MQ01-13
  this.GreenRoom = function() {
    // na poczatku pusty
    this.init_board();
    this.board.set_color('green');
    this.board.set_title('Green Room');
  };

  // MQ01-48
  this.RedRoom = function() {
    this.init_board();
    this.board.create_character(7, 0, 'chest');
    this.board.set_color('red');
    this.board.set_title('Red Room');
  };

  // MQ01-57
  this.WhiteRoom = function() {
    this.init_board();
    this.board.set_color('white');
    this.board.set_title('White Room');
    this.set_types_center([0, 1, 2, 3, 5, 9, 11, 12, 13, 14], 'black');
  };

  // MQ01-71
  this.AzureRoom = function() {
    this.init_board();
    this.board.set_color('Azure');
    this.board.set_title('Azure Room');
    var blacks = [
      [2, 1],

      [0, 0],
      [0, 3],
      [0, 4],

      [7, 0],

      [14, 0],
      [14, 6],
      [14, 1],

      [12, 4],
    ];
    this.set_types(blacks, 'black');
    this.board.create_character(2, 6, 27);
    this.board.create_character(7, 2, 27);
    this.board.create_character(7, 5, 27);
    this.board.create_character(12, 3, 27);
  };

  // MQ01-75
  this.YellowRoom = function() {
    this.init_board();
    this.board.set_color('yellow');
    this.board.set_title('Yellow Room');
    this.set_types_megahex([7], 'grey');
    this.board.create_character(1, 0, 1);
    this.board.create_character(6, 0, 2);
    this.board.create_character(12, 0, 3);
  };

  // MQ01-159
  this.BlackRoom = function() {
    this.init_board();
    this.board.set_color('black');
    this.board.set_title('Black Room', 'white');
    this.set_types_megahex([1, 13], 'black');
    this.set_types_center([0, 2, 12, 14], 'black');
    this.board.create_character(2, 6, 1);
    this.board.create_character(7, 0, 2);
    this.board.create_character(7, 2, 3);
    this.board.create_character(7, 5, 4);
  };

  this.get_neighbors = function(mh_id, id) {
    console.log(mh_id + ' ' + id);
    //var mh_total = this.board.width * this.board.height;
    var mh_x = this.board.megahexes[mh_id].x;
    var mh_y = this.board.megahexes[mh_id].y;
    var mh_max_x = this.board.height - 1;
    var mh_max_y = this.board.width -1;
    
    var mh_top_left = null;
    var mh_top_right = null;
    var mh_left = null;
    var mh_right = null;
    var mh_bottom_left = null;
    var mh_bottom_right = null;
    
    if (mh_y > 0) {
      if (mh_x > 0) {
        mh_top_left = this.board.x_y_2_id(mh_x-1, mh_y-1);
      };
      mh_top_right = this.board.x_y_2_id(mh_x, mh_y-1);
    };
    
    if (mh_x > 0) {
      mh_left = this.board.x_y_2_id(mh_x-1, mh_y);
    };
    if (mh_x < mh_max_x) {
      mh_right = this.board.x_y_2_id(mh_x+1, mh_y);
    };

    if (mh_y < mh_max_y) {
      if (mh_x < mh_max_x) {
        mh_bottom_right = this.board.x_y_2_id(mh_x+1, mh_y+1);
      };
      mh_bottom_left = this.board.x_y_2_id(mh_x, mh_y+1);
    };
    
    console.log('top left: ' + mh_top_left);
    console.log('top right: ' + mh_top_right);
    console.log('left: ' + mh_left);
    console.log('right: ' + mh_right);
    console.log('bottom left: ' + mh_bottom_left);
    console.log('bottom right: ' + mh_bottom_right);
    
    var neighbors = [];
    var neighbors_push = function(mh, hexes) {
      for (var i in hexes) {
        var to_push = null;
        if (mh !== null) {
          to_push = [mh, hexes[i]];
        };
        neighbors.push(to_push);
      };
    }
    
    if (id == 0) {
      neighbors_push(mh_id, [1, 2, 3, 4, 5, 6]);
    } else if (id == 1) {
      neighbors_push(mh_top_left, [4, 5]);
      neighbors_push(mh_id, [2, 0, 6]);
      neighbors_push(mh_left, [3]);
    } else if (id == 2) {
      neighbors_push(mh_top_left, [4]);
      neighbors_push(mh_top_right, [5, 6]);
      neighbors_push(mh_id, [1, 0, 3]);
    } else if (id == 3) {
      neighbors_push(mh_id, [2]);
      neighbors_push(mh_top_right, [5]);
      neighbors_push(mh_right, [1, 6]);
      neighbors_push(mh_id, [4, 0]);      
    } else if (id == 4) {
      neighbors_push(mh_id, [0, 3]);
      neighbors_push(mh_right, [6]);
      neighbors_push(mh_bottom_right, [2, 1]);
      neighbors_push(mh_id, [5]);
    } else if (id == 5) {
      neighbors_push(mh_id, [6, 0, 4]);
      neighbors_push(mh_bottom_right, [1]);
      neighbors_push(mh_bottom_left, [3, 2]);
    } else if (id == 6) {
      neighbors_push(mh_left, [3]);
      neighbors_push(mh_id, [1, 0, 5]);
      neighbors_push(mh_bottom_left, [2]);
      neighbors_push(mh_left, [4]);
    };
    
    
    var str_neighbors = '';
    for (var i in neighbors) {
      str_neighbors = str_neighbors + '(' + neighbors[i] + ') ';
    };
    console.log(str_neighbors);
    return neighbors; 
  };
  
  this.show_neighbors = function(neighbors, rotation=null) {
    var type = 'yellow';
    for (var idx in neighbors) {
      var n = neighbors[idx];
      if (n !== null) {
        if (rotation !== null) {
          var side = parseInt(idx) - rotation;
          if (side < 0) {
            side += 6;
          };
          console.log(side); 
          switch (side) {
            case 0:
            case 1:
            case 2: 
              type = 'yellow_f';
              break;
            case 3:
            case 5:
              type = 'yellow_s';
              break;
            case 4:
              type = 'yellow_r';
              break;
            default:
              type = 'yellow';          
          };  
        };
        this.board.megahexes[n[0]].hexes[n[1]].set_type(type);
      };      
    };
  };

  this.create_game();
};

