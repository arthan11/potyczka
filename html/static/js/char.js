Char = function(hex, id, img) {
  if (img == 28) { // olbrzym
    this.width = 120;
    this.height = 104;
  } else {
    this.width = 60;
    this.height = 60;
  };
  this.hex = hex;
  this.board = hex.parent.parent;
  this.id = id;
  this.img = img;
  this.object = null;
  this.x = 0;
  this.y = 0;
  this.rotation = 0;
  this.angle = 0;
  this.name = '';
  this.race = '';
  this.str = 0;
  this.dex = 0;
  this.dex_adj = 0;
  this.dex_adj2 = 0;
  this.ma = 0;
  this.exp = 0;

  this.create_char = function() {
    this.object = $( '<div class="char char_' + this.img + '" id="char_' + this.id + '"></div>' );
    this.object.appendTo(this.board.object);
    this.move(this.hex);
  };

  this.rotate = function(angle) {
    //$( this.object ).css({'transform': 'rotate(' + angle*60 + 'deg)'});
    this.calc_rotation(angle);
    this.calc_xy(this.hex);
    $(this.object).velocity({
      'left': this.x,
      'top': this.y,
      rotateZ: this.angle + "deg"
    });
    var neighbors = game.get_neighbors(hex.parent.id, hex.id);
    game.show_neighbors(neighbors, this.rotation);
  };

  this.calc_rotation = function(rotation) {
    this.rotation = rotation;
    if (this.img == 28) {
      if (rotation == 0) {
        this.angle = 90;
      } else if (rotation == 1) {
        this.angle = -150;
      } else if (rotation == 2) {
        this.angle = -30;
      } else if (rotation == 3) {
        this.angle = 30;
      } else if (rotation == 4) {
        this.angle = 150;
      } else if (rotation == 5) {
        this.angle = -90;
      } else {
        this.angle = 0;
      };
    } else {
      this.angle = rotation * 60;
    };
  };

  this.calc_xy = function(hex) {
    this.hex = hex;
    this.x = this.hex.x + (this.board.hex_width - this.width) * 0.5;
    this.y = this.hex.y + (this.board.hex_height - this.height) * 0.5;

    if (this.img == 28) {
      if (this.rotation == 0) {
        this.x = this.x + this.board.hex_width * 0.5;
        this.y = this.hex.y + this.board.hex_height * 0.40;
      } else if (this.rotation == 1) {
        this.x = this.x + this.board.hex_width * 0.20;
        this.y = this.hex.y + this.board.hex_height * 0.55;
      } else if (this.rotation == 2) {
        this.x = this.x + this.board.hex_width * 0.25;
        this.y = this.hex.y + this.board.hex_height * 0.20;
      } else if (this.rotation == 3) {
        this.x = this.x - this.board.hex_width * 0.15;
        this.y = this.hex.y + this.board.hex_height * 0.20;
      } else if (this.rotation == 4) {
        this.x = this.x - this.board.hex_width * 0.15;
        this.y = this.hex.y + this.board.hex_height * 0.55;
      } else if (this.rotation == 5) {
        this.x = this.x - this.board.hex_width * 0.45;
        this.y = this.hex.y + this.board.hex_height * 0.40;
      };
    };
  };

  this.move = function(hex) {
    //$( this.object ).css({'left': x, 'top': y});
    this.calc_xy(hex);
    $(this.object).velocity({
      'left': this.x,
      'top': this.y
    });
    var neighbors = game.get_neighbors(hex.parent.id, hex.id);
    game.show_neighbors(neighbors, this.rotation);
  };

  this.move_and_rotate = function(hex, angle) {
    this.calc_rotation(angle);
    this.calc_xy(hex);
    $(this.object).velocity({
      'left': this.x,
      'top': this.y,
      rotateZ: this.angle + "deg"
    });
    var neighbors = game.get_neighbors(hex.parent.id, hex.id);
    game.show_neighbors(neighbors, this.rotation);
  };

  this.destroy = function() {
    this.object.remove();
    var chars = this.board.characters;
    for (var i = 0; i < chars.length; i++) {
      if (chars[i] === this) {
        chars.splice(i, 1);
        break;
      };
    };
  };

  this.show_record_sheet = function() {
    if (this.board.record_sheet_object) {
      this.board.record_sheet_object.remove();
    };
    this.board.record_sheet_object = $( '<div class="hidden record_sheet"></div>' );
    this.board.record_sheet_object.appendTo(this.board.parent);
    $('<div id="name">' + this.name + '</div>' ).appendTo(this.board.record_sheet_object);
    $('<div id="race">' + this.race + '</div>' ).appendTo(this.board.record_sheet_object);
    $('<div id="str">' + this.str + '</div>' ).appendTo(this.board.record_sheet_object);

    $('<div id="dex">' + this.dex + '</div>' ).appendTo(this.board.record_sheet_object);
    $('<div id="dex_adj">' + this.dex_adj + '</div>' ).appendTo(this.board.record_sheet_object);
    if (this.dex_adj2 > 0) {
      $('<div id="dex_adj2">' + this.dex_adj2 + '</div>' ).appendTo(this.board.record_sheet_object);
    };
    $('<div id="ma">' + this.ma + '</div>' ).appendTo(this.board.record_sheet_object);
    $('<div id="exp">' + this.exp + '</div>' ).appendTo(this.board.record_sheet_object);

    this.board.record_sheet_object.velocity("slideDown")
  };

  this.create_char();

};

