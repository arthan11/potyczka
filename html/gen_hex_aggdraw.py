import math
from PIL import Image, ImageDraw
from aggdraw import Draw, Brush, Pen, Font
from time import sleep


back_color = (255, 0, 255, 0)
pen_color = 'black'
brush_color = 'white'
a = 50
line_h = 6

def hexagon_generator(edge_length, offset):
    """Generator for coordinates in a hexagon."""
    x, y = offset
    for angle in range(0, 360, 60):
        x += math.cos(math.radians(angle)) * edge_length
        y += math.sin(math.radians(angle)) * edge_length
        yield x
        yield y

if __name__ == '__main__':
    w = 2 * a
    h = int(math.ceil(a * math.sqrt(3)))
    x = a / 2

    def create_hex(name, line_pos=None, text=None):
        image = Image.new('RGBA', (w, h), back_color)
        draw = Draw(image)
        hexagon = hexagon_generator(a, offset=(x, 0))
        draw.polygon(list(hexagon), Pen(pen_color), Brush(brush_color))
        draw.flush()

        if line_pos != None:
            p = Pen("black", line_h)
            hex = list(hexagon_generator(a, offset=(x, 0)))
            hex += hex
            p1 = line_pos
            p2 = line_pos+4
            line = hex[line_pos:line_pos+4]
            draw.line(line, p)

            line = hex[line_pos+2:line_pos+6]
            draw.line(line, p)

            line = hex[line_pos+4:line_pos+8]
            draw.line(line, p)

            draw.flush()

        if text:
            font = Font('blue', 'arialbd.ttf', size=40)
            pos_x = int(float(a)*0.80)
            pos_y = int(float(a)*0.35)
            draw.text((pos_x, pos_y), text, font)
            draw.flush()
        
        image.save(name)

    '''
    create_hex('hex0.png')
    create_hex('hex4.png', line_pos=0)
    create_hex('hex5.png', line_pos=2)
    create_hex('hex6.png', line_pos=4)
    create_hex('hex1.png', line_pos=6)
    create_hex('hex2.png', line_pos=8)
    create_hex('hex3.png', line_pos=10)

    brush_color = 'grey'
    create_hex('hex_grey.png')
    brush_color = 'black'
    create_hex('hex_black.png')
    '''
    brush_color = 'yellow'
    create_hex('hex_yellow_f.png', text='f')
    create_hex('hex_yellow_s.png', text='s')
    create_hex('hex_yellow_r.png', text='r')