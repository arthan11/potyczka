# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class TypBroni(models.Model):
    nazwa = models.CharField(max_length=20)
    class Meta:
        ordering = ['nazwa']
    def __unicode__(self):
        return self.nazwa

class Bron(models.Model):
    typ = models.ForeignKey('TypBroni')
    nazwa = models.CharField(max_length=20)
    atak = models.IntegerField()
    atak_mod = models.IntegerField(null=True, blank=True) 
    wymagana_sila = models.IntegerField(null=True, blank=True)
    rzucana = models.BooleanField(default=False)
    dwureczna = models.BooleanField(default=False)  
    class Meta:
        ordering = ['typ', 'nazwa']
    def __unicode__(self):
        ret = 'broń {} - {} [{}K6'.format(self.typ, self.nazwa, self.atak)
        if self.atak_mod:
            ret += str(self.atak_mod)
        ret += ']'
        if self.wymagana_sila:
            ret += ', S: {}'.format(self.wymagana_sila)
        if self.rzucana:
            ret += ', rzucana'            
        if self.dwureczna:
            ret += ', dwureczna'            
        return ret

class SprzetObronny(models.Model):
    typ = models.ForeignKey('TypBroni')
    nazwa = models.CharField(max_length=20)
    ochrona = models.IntegerField()
    pr = models.IntegerField()
    z_mod = models.IntegerField()
    class Meta:
        ordering = ['typ', 'nazwa']
    def __unicode__(self):
        return u'{} - {} ({}), PR:{}, Z:{}'.format(self.typ, self.nazwa, self.ochrona, self.pr, self.z_mod)        

class TypCzaru(models.Model):
    nazwa = models.CharField(max_length=20)
    class Meta:
        ordering = ['nazwa']
    def __unicode__(self):
        return self.nazwa

class Czar(models.Model):
    typ = models.ForeignKey('TypCzaru')
    nazwa = models.CharField(max_length=20)
    poziom_iq = models.IntegerField()
    opis = models.TextField()
    class Meta:
        ordering = ['poziom_iq', 'nazwa']
    def __unicode__(self):
        typ = self.typ.nazwa.split(' ')[-1][0].upper()
        return '[IQ:{}] {}({})'.format(self.poziom_iq, self.nazwa, typ)

class Rasa(models.Model):
    nazwa = models.CharField(max_length=20)
    sila = models.IntegerField()
    zrecznosc = models.IntegerField()
    inteligencja = models.IntegerField(blank=True, null=True)
    punkty_ruchu = models.IntegerField()
    punkty_wolne = models.IntegerField(blank=True, null=True)
    class Meta:
        ordering = ['nazwa']
    def __unicode__(self):
        return self.nazwa
        
class Postac(models.Model):
    rasa = models.ForeignKey('Rasa')
    nazwa = models.CharField(max_length=20)
    sila = models.IntegerField()
    zrecznosc = models.IntegerField()
    zmodyfikowana_zrecznosc = models.IntegerField()
    inteligencja = models.IntegerField(blank=True, null=True)
    punkty_wolne = models.IntegerField()
    punkty_ruchu = models.IntegerField()
    punkty_doswiadczenia = models.IntegerField(default=0)
    rany = models.IntegerField(default=0)
    bron = models.ManyToManyField('Bron', blank=True)
    sprzet_obronny = models.ManyToManyField('SprzetObronny', blank=True)
    class Meta:
        ordering = ['nazwa']
    def __unicode__(self):
        return str(self.nazwa)

class TabelaWojownikow(models.Model):
    postac = models.ForeignKey('Postac')
    nr = models.IntegerField()
    def opis(self):
        ret = ''
        if self.postac.rasa.nazwa != 'człowiek':
            ret += '{}. '.format(self.postac.rasa.nazwa.capitalize())
        ret += 'S = {}, Z = {}'.format(self.postac.sila, self.postac.zrecznosc)
        if self.postac.zmodyfikowana_zrecznosc != self.postac.zrecznosc:
            ret += ' ({})'.format(self.postac.zmodyfikowana_zrecznosc) 
        if self.postac.inteligencja != 8:
            ret += ', IQ = {}.'.format(self.postac.inteligencja) 
        obronne = [bron.nazwa.lower() for bron in self.postac.sprzet_obronny.all()]
        bronie = [bron.nazwa.lower() for bron in self.postac.bron.all()]
        if obronne:
            ret += ' ' + ', '.join(obronne).capitalize()
        if bronie:
            if obronne:
                ret += ', ' + ', '.join(bronie)
            else:
                ret += ' ' + ', '.join(bronie).capitalize()
        return ret
    class Meta:
        ordering = ['nr']
    def __unicode__(self):
        return u'{}: {}'.format(self.nr, self.opis())

class StatusPotyczki(models.Model):
    etap = models.IntegerField()
    


class Step(models.Model):
    nr = models.IntegerField()
    text = models.TextField()
    text_pl = models.TextField(null=True, blank=True)
    class Meta:
        ordering = ['nr']
    def __unicode__(self):
        ret = str(self.nr)
        if self.text_pl:
            ret += ' [PL]'
        return ret

class Way(models.Model):
    step_from = models.ForeignKey('Step', related_name='ways_from')
    step_to = models.ForeignKey('Step', related_name='ways_to')
    caption = models.CharField(max_length=50, null=True, blank=True)
    caption_pl = models.CharField(max_length=50, null=True, blank=True)
    class Meta:
        ordering = ['step_from', 'step_to']
    def __unicode__(self):
        if self.caption_pl:
            return u'{} -> {}: {}'.format(self.step_from, self.step_to, self.caption_pl)
        else:
            return u'{} -> {}'.format(self.step_from, self.step_to)