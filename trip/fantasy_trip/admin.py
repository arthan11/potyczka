from django.contrib import admin
from fantasy_trip.models import *


admin.site.register(Step)
admin.site.register(Way)

admin.site.register(TypBroni)
admin.site.register(Bron)
admin.site.register(SprzetObronny)
admin.site.register(TypCzaru)
admin.site.register(Czar)
admin.site.register(Rasa)
admin.site.register(Postac)
admin.site.register(TabelaWojownikow)