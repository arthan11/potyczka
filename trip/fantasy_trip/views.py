from django.shortcuts import render_to_response
from django.http import JsonResponse
from fantasy_trip.models import *


def get_step(request, step_nr):

    step = Step.objects.get(nr=step_nr)
    ret = {
      'id': step.id,
      'nr': step.nr,
      'text': step.text,
      'text_pl': step.text_pl,
      'ways': []
    }
    for w in step.ways_from.all():
        caption = w.caption
        caption_pl = w.caption_pl
        if caption:
            caption = caption.capitalize()
        if caption_pl:
            caption_pl = caption_pl.capitalize()
        way = {
            'caption': caption,
            'caption_pl': caption_pl,
            'nr': w.step_to.nr,
        }
        ret['ways'].append(way)
    return JsonResponse(ret)

def index(request):
    params = {}
    params['first_step'] = Step.objects.get(nr=1)
    return render_to_response('index.html', params)