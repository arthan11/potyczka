# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-22 10:55
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fantasy_trip', '0007_auto_20170222_1152'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bron',
            name='atak',
        ),
        migrations.RemoveField(
            model_name='bron',
            name='atak_mod',
        ),
        migrations.RemoveField(
            model_name='bron',
            name='dwureczna',
        ),
        migrations.RemoveField(
            model_name='bron',
            name='rzucana',
        ),
        migrations.RemoveField(
            model_name='bron',
            name='wymagana_sila',
        ),
    ]
