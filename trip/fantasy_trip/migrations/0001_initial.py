# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-04 13:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Step',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.IntegerField()),
                ('text', models.CharField(max_length=100)),
            ],
        ),
    ]
