# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-22 10:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasy_trip', '0006_auto_20170222_1149'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='typbroni',
            options={'ordering': ['nazwa']},
        ),
        migrations.AlterField(
            model_name='bron',
            name='atak_mod',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='bron',
            name='wymagana_sila',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
